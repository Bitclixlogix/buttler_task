import React from 'react';

const ClientIdTable = (props) => {
    const {requests} = props;

    const renderTbaleBody = () => {
        let tableRow = [];
        if (Array.isArray(requests) && requests.length > 0 ) {
            requests.map((client, index) => {
                tableRow.push(
                    <tr key={index}>
                        <td>{index+1}</td>
                        <td>{client.clientId}</td>
                        <td>{client.requestId}</td>
                        <td>{client.hours}</td>
                    </tr>
                )
            })
        };
        return tableRow
    };

    return (
        <>
            <h4>Client Table</h4>
            <div className="client-table client-table-head">
                <table className="table table-striped table-bordered mb-0">
                    <thead>
                    <tr>
                        <th>Sr No.</th>
                        <th>Client Id</th>
                        <th>Request Id</th>
                        <th>Hours</th>
                    </tr>
                    </thead>
                </table>
            </div>

            {
                Array.isArray(requests) &&
                requests.length > 0 ?
                    <div className="client-table mb-4">
                        <table className="table table-striped table-bordered">
                            <tbody>
                            {renderTbaleBody()}
                            </tbody>
                        </table>
                    </div>:
                    <div className="no-data">
                        <span>No Data</span>
                    </div>
            }
        </>
    );
};

export default ClientIdTable;