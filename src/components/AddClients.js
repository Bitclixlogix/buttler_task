import React, {useState} from 'react';

const AddClients = React.memo(function AddClients(props) {
    const [clientData, setClientData] = useState({
        clientId: "",
        requestId: '',
        hours: 0
    });

    const handdleInputChange = ({target}) => {
        if (target.name === "hours" ){
            if (target.value > 8) {
                alert("Please Add hours less then 8 or 8")
            } else {
                setClientData(prevState => {
                    return {...prevState, [target.name]: parseInt(target.value)}
                })
            }
        } else {
            setClientData(prevState => {
                return {...prevState, [target.name]: target.value}
            })
        }
    };

    const addClient = (e) => {
        e.preventDefault();
        if (clientData.clientId === "") {
            alert('Please add Client id')
        } else if (clientData.requestId === "") {
            alert('Please add Request id')
        } else if (clientData.hours === "") {
            alert('Please add working hours')
        } else {
            props.addClient(clientData)
            setClientData({
                clientId: "",
                requestId: '',
                hours: 0
            })
        }
    };

    return (
        <div className="add-client">
            <h4>Add Clients</h4>
            <div className="container">
                <form>
                    <div className="form-group">
                        <label>Client Id:</label>
                        <input type="text"
                               className="form-control"
                               placeholder="Client Id"
                               name="clientId"
                               value={clientData.clientId}
                               onChange={handdleInputChange}
                        />
                    </div>
                    <div className="form-group">
                        <label >Request Id:</label>
                        <input type="text"
                               className="form-control"
                               placeholder="Enter Request Id"
                               name="requestId"
                               value={clientData.requestId}
                               onChange={handdleInputChange}
                        />
                    </div>
                    <div className="form-group">
                        <label >Hours:</label>
                        <input type="number"
                               className="form-control"
                               placeholder="Enter working hours"
                               name="hours"
                               value={clientData.hours}
                               onChange={handdleInputChange}/>
                    </div>
                    <button className="btn btn-primary"
                            type="submit"
                            onClick={addClient}>
                        Submit
                    </button>
                </form>
            </div>
        </div>
    );
});

export default AddClients;