import React from 'react';

const AssignedButlers = ({assignedButlers}) => {
    let requests = assignedButlers === null ? [] : assignedButlers.butlers;
    return (
        <>
            <h4>Assigned Butlers</h4>
            <div className="client-table client-table-head">
                <table className="table table-striped table-bordered mb-0">
                    <thead>
                    <tr>
                        <th>Sr No.</th>
                        <th>Client Requests</th>
                    </tr>
                    </thead>
                </table>
            </div>
            {
                Array.isArray(requests) &&
                requests.length > 0 ?
                    <div className="client-table">
                        <table className="table table-striped table-bordered">
                            <tbody>
                            {
                                Array.isArray(requests) &&
                                requests.length > 0 &&
                                requests.map((client, index) => {
                                    return <tr key={index}>
                                        <td width="50%">{index+1}</td>
                                        <td width="50%">{client.requests.join(", ")}</td>
                                    </tr>
                                })
                            }
                            </tbody>
                        </table>
                    </div>:
                    <div className="no-data">
                        <span>No Data</span>
                    </div>
            }
        </>
    );
};

export default AssignedButlers;