import React, {useState} from 'react';
import AddClients from "./AddClients";
import ClientIdTable from "./ClientIdTable";
import AssignedButlers from "./AssignedButlers";

const Butlers = () => {
    const exampleRequests = [
    ];


    const [requests, setRequests] = useState(exampleRequests)
    const [assignedButlers, setAssignedButlers] = useState(null)

    const allocateAndReport = () => {
        let butlers = [
            { butlerId: 1, requestId: [], hours: 0 }
        ];

        requests.sort((a, b) => a.clientId - b.clientId);
        let clientIds = [];
        let index = 2;
        for (const request of requests) {
            let checkbutler=false;
            for (const butler of butlers) {
                let hours = butler.hours + request.hours;
                if (butler.hours < 8 && hours <=8) {
                    let objIndex = butlers.findIndex((obj => obj.butlerId == butler.butlerId))
                    butlers[objIndex].hours= hours;
                    butlers[objIndex].requestId.push(request.requestId);
                    clientIds.push(request.clientId);
                    checkbutler = true;
                    break;
                }
            }

            if(!checkbutler){
                butlers=[...butlers,{ butlerId: index, requestId: [request.requestId], hours: request.hours }]
                index +=1;
            }
        }

        let resultantArray = {
            butlers: [],
            spreadClientIds: [...new Set(clientIds)]
        };
        for (const butler of butlers){
            resultantArray.butlers.push({
                requests: butler.requestId
            })
        }
        setAssignedButlers(resultantArray)
        console.log("resultantArray", resultantArray);

    };

    const addClient = (data) => {
        let cloneRequests = [...requests];
        cloneRequests.push(data);
        setRequests(cloneRequests)
    };

    return (
        <div className="container">
            <div className="heading">
                <h1>Buttlers</h1>
            </div>
            <div className="buttlers">
                <AddClients addClient={addClient}/>
                <div className="tables">
                    <ClientIdTable requests={requests}/>
                    <AssignedButlers assignedButlers={assignedButlers}/>
                </div>
            </div>
            <div className="allocate-btn">
                <button className="btn btn-primary" onClick={allocateAndReport}>
                    Allocate And Report
                </button>
            </div>
        </div>
    );
};

export default Butlers;